﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SnakeGrid
{
    private GridPosition[,] m_positions;
    private Transform grid;
    private int m_height;
    private int m_width;
    public SnakeGrid(
        Vector3 centerPosition,
        int width,
        int height,
        float dotWidth, 
        Sprite gritDotBackground
    )
    {
        if (width % 2 != 0) width++;
        if (height % 2 != 0) height++;

        m_height = height;
        m_width = width;
        
        Vector3 delta0Pos = Vector3.zero;
        delta0Pos.x = -( width*dotWidth / 2);
        delta0Pos.y = (height*dotWidth) / 2;

        Vector3 initalPos = centerPosition + delta0Pos + new Vector3(dotWidth/2,-dotWidth/2,0);
        m_positions = new GridPosition[width,height];
        
        //Destroy previous grid
        if(grid!=null) GameObject.Destroy(grid.gameObject);
        
        GameObject gridGo = new GameObject("[Grid]");
        grid = gridGo.transform;
        grid.position = centerPosition;
        GameObject wallsGo = new GameObject("walls");
        wallsGo.transform.parent = gridGo.transform;
        
        for (int y = 0; y < height; y++)
        {
            GameObject row = new GameObject(y.ToString());
            row.transform.parent = grid;
            
            for (int x = 0; x < width; x++)
            {
                Vector3 currPos = initalPos + new Vector3(x*dotWidth,-y*dotWidth,0);
                GameObject gridDot = new GameObject(x.ToString());
                gridDot.transform.parent = row.transform;
                gridDot.transform.position = currPos;
                //SpriteRenderer spriteRend = gridDot.AddComponent<SpriteRenderer>();
                //spriteRend.sprite = gritDotBackground;
                float scale = 0.5f;
                gridDot.transform.localScale = new Vector3(scale,scale,1);
                //spriteRend.size = new Vector2(dotWidth,dotWidth);
                //var color = spriteRend.color;
                //color.a = 0.3f;
                //spriteRend.color = color;
                
                m_positions[x,y] = new GridPosition(x,y,currPos,gridDot);


                Vector3 wallPos = currPos;
                
                if (x == 0 || x==width-1 || y==0 || y==height-1)
                {
                    if (y==0) wallPos.y += dotWidth;
                    else if(y==height-1) wallPos.y -= dotWidth;
                    else if(x==0)wallPos.x -= dotWidth;
                    else if(x==width-1) wallPos.x += dotWidth;
                    
                    var wall = CreateWallAsset(
                        wallsGo.transform,
                        gritDotBackground,
                        wallPos
                    );
                    
                    //Extra Walls and corners
                    if ((y == 0 || y == height - 1)&&(x==0 || x==width-1))
                    {
                        wallPos = currPos;
                        if (x == 0) wallPos.x -= dotWidth;
                        if (x==width-1) wallPos.x += dotWidth;
                        //Extrawall
                        var extraWall = CreateWallAsset(
                            wallsGo.transform,
                            gritDotBackground,
                            wallPos
                        );
                        //Corner
                        if (y == 0) wallPos.y += dotWidth;
                        if (y == width-1) wallPos.y -= dotWidth;
                        
                        var cornerWall = CreateWallAsset(
                            wallsGo.transform,
                            gritDotBackground,
                            wallPos
                        );
                    }
                }
            }
        }
    }

    private Transform CreateWallAsset(Transform Parent, Sprite asset, Vector3 position)
    {
        GameObject wallsGo = new GameObject("wall");
        wallsGo.transform.parent = Parent;
        SpriteRenderer spriteRend = wallsGo.AddComponent<SpriteRenderer>();
        spriteRend.sprite = asset;
        wallsGo.transform.position = position;
        spriteRend.sortingOrder = 1;
        return wallsGo.transform;
    }

    public static Vector2Int up => Vector2Int.down;
    public static Vector2Int down => Vector2Int.up;
    public static Vector2Int left => Vector2Int.left;
    public static Vector2Int right => Vector2Int.right;
    
    public GridPosition GetCenterPos()
    {
        return m_positions[m_width / 2, m_height / 2];
    }

    public GridPosition GetPositionFromVector(GridPosition initPos, Vector2Int movementVector)
    {
        Vector2Int destPos = initPos.GetGridPos() + movementVector;
        if (destPos.x >= 0 && destPos.x < m_width && destPos.y >= 0 && destPos.y < m_height) return m_positions[destPos.x,destPos.y];
        return null;
    }

    public GridPosition GetRandomPos(List<GridPosition> excludedPos)
    {
        Dictionary<int, HashSet<int>> excluded = new Dictionary<int, HashSet<int>>();
        foreach (var gridPosition in excludedPos)
        {
            var pos = gridPosition.GetGridPos();
            if (!excluded.ContainsKey(pos.x))
            {
                HashSet<int> set = new HashSet<int>();
                set.Add(pos.y);
                excluded.Add(pos.x,set);
            }
            else
            {
                var hash = excluded[pos.x];
                hash.Add(pos.y);
            }
        }
        List<Vector2Int> positions = new List<Vector2Int>();
        for (int x = 0; x < m_width; x++)
        {
            for (int y = 0; y < m_height; y++)
            {
                if(!(excluded.ContainsKey(x) && excluded[x].Contains(y))) positions.Add(new Vector2Int(x,y));
            }
        }

        int index = UnityEngine.Random.Range(0, positions.Count);
        Vector2Int posInt = positions[index];
        return m_positions[posInt.x,posInt.y];
    }

    public void AutoDestroy()
    {
        foreach (var gridPosition in m_positions)
        {
            gridPosition.AutoDestroy();
        }
        m_positions = null;
        
        GameObject.Destroy(grid.gameObject);
    }
}