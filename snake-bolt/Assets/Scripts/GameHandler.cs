﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = System.Random;

public class GameHandler : MonoBehaviour
{
    // References
    public Assets assets;
    public Canvas canvas;
    
    // Game Classes
    private SnakeGrid grid;
    private Snake snake;
    private EatDot eatDot;
    
    // Handler data
    private float movementTimer = 0;
    private Vector2Int movementVector = SnakeGrid.up;
    private bool m_userLose = true;
    private int m_userScore = 0;
    private int maxUserScore => GetUserMaxScore();

    private int GetUserMaxScore()
    {
        return !PlayerPrefs.HasKey("MAXSCORE") ? 0 : PlayerPrefs.GetInt("MAXSCORE");
    }

    private void SetUserMaxScore(int score)
    {
        PlayerPrefs.SetInt("MAXSCORE",score);
    }

    private void Awake() {
        
        canvas.inGame.mainMenuAction = GoToMainMenuAction;
        canvas.newGame.newGameAction = NewGameAction;

        GoToMainMenuAction();
    }

    private void NewGameAction()
    {
        m_userScore = 0;
        
        grid?.AutoDestroy();
        grid = new SnakeGrid(transform.position, 20, 20, 1, assets.whiteDot);
        GridPosition pos = grid.GetCenterPos();

        snake?.AutoDestroy();
        snake = new Snake(pos,assets.snakeHead,assets.snakeTail);
        snake.OnSnakeEatDot = OnSnakeEatDot;
        snake.OnSnakeEatWallOrSnake = OnSnakeEatWallOrSnake;

        eatDot?.AutoDestroy();
        eatDot = new EatDot(pos,assets.eatDot);
        MoveEatDot();
        
        m_userLose = false;
        
        canvas.inGame.UpdateUserScore(m_userScore);
        canvas.inGame.UpdateMaxScore(GetUserMaxScore());
        
        canvas.inGame.Show();
        canvas.newGame.Show(false);
    }

    private void GoToMainMenuAction()
    {
        if(m_userScore>=GetUserMaxScore()) SetUserMaxScore(m_userScore);
        canvas.newGame.SetMaxScore(GetUserMaxScore());
        
        m_userScore = 0;
        m_userLose = true;
        canvas.inGame.Show(false);
        canvas.newGame.Show();
    }

    private void OnSnakeEatWallOrSnake() => GoToMainMenuAction();

    private void OnSnakeEatDot()
    {
        m_userScore += 1;
        canvas.inGame.UpdateUserScore(m_userScore);
        MoveEatDot();
    }

    public void Update()
    {
        if(m_userLose) return;
        
        movementTimer += Time.deltaTime;
        if (Input.GetKeyUp(KeyCode.UpArrow)) movementVector = SnakeGrid.up;
        if (Input.GetKeyUp(KeyCode.DownArrow)) movementVector = SnakeGrid.down;
        if (Input.GetKeyUp(KeyCode.LeftArrow)) movementVector = SnakeGrid.left;
        if (Input.GetKeyUp(KeyCode.RightArrow)) movementVector = SnakeGrid.right;
        
        if (movementTimer >= 0.2f)
        {
            movementTimer = 0;
            GridPosition snakePos = snake.GetPosition();
            GridPosition gp = grid.GetPositionFromVector(snakePos,movementVector);
            if (gp == null)
            {
                OnSnakeEatWallOrSnake();
                return;
            }
            
            snake.MoveTo(gp);
        }
    }

    public void MoveEatDot()
    {
        List<GridPosition> excludedPos = new List<GridPosition>();
        excludedPos.AddRange(snake.GetUsedPartsPositions());
        GridPosition randomPosOnGrid = grid.GetRandomPos(excludedPos);
        eatDot.MoveToPos(randomPosOnGrid);
    }

    [Serializable]
    public class Assets
    {
        public Sprite whiteDot;
        public Sprite snakeHead;
        public Sprite snakeTail;
        public Sprite eatDot;
    }

    [Serializable]
    public class Canvas
    {
        public IngameCanvas inGame;
        public NewGameCanvas newGame;
    }
}