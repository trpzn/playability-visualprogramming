﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Snake
{
    private SnakeDot m_head;
    public List<SnakeDot> snakeParts;
    private Sprite m_headSprite;
    private Sprite m_tailSprite;
    private GameObject snakePartsGO;
    public Action OnSnakeEatDot;
    public Action OnSnakeEatWallOrSnake;

    public Snake(
        GridPosition initialPos, 
        Sprite headSprite, 
        Sprite tailSprite
    )
    {
        m_headSprite = headSprite;
        m_tailSprite = tailSprite;
        Vector3 headPos = initialPos.GetPosition();
        
        snakeParts = new List<SnakeDot>();
        if(snakePartsGO !=null) GameObject.Destroy(snakePartsGO);
        snakePartsGO = new GameObject("[SnakeParts]");
        
        GameObject snakeHeadObject = CreateSnakeHeadGO(headPos);
        snakeHeadObject.transform.parent = snakePartsGO.transform;
        
        m_head = new SnakeDot(snakeHeadObject,initialPos);
        snakeParts.Add(m_head);
    }

    GameObject CreateSnakeHeadGO(Vector3 position)
    {
        GameObject go = new GameObject("Head");
        SpriteRenderer sprite = go.AddComponent<SpriteRenderer>();
        sprite.sprite = m_headSprite;
        sprite.sortingOrder = 2;
        go.transform.position = position;
        go.transform.parent = snakePartsGO.transform;
        return go;
    }
    
    GameObject CreateSnakeTaleGO(Vector3 position)
    {
        GameObject go = new GameObject("Tale");
        SpriteRenderer sprite = go.AddComponent<SpriteRenderer>();
        sprite.sprite = m_tailSprite;
        sprite.sortingOrder = 2;
        //sprite.sprite = m_headSprite;
        go.transform.position = position;
        go.transform.parent = snakePartsGO.transform;
        return go;
    }

    public GridPosition GetPosition() => m_head.GetPosition();

    public void MoveTo(GridPosition gp)
    {
        
        if(gp.IsOccupiedBy(typeof(SnakeDot)))
        {
            OnSnakeEatWallOrSnake?.Invoke();
            return;
        }
        
        GridPosition lastPosition = m_head.GetPosition();
        //Move head to new position
        m_head.MoveTo(gp);
        
        if (gp.IsOccupiedBy(typeof(EatDot)))
        {
            OnSnakeEatDot?.Invoke();
            EatDotOnCurrPos();
        }
        
        if (m_head.isEatDot)
        {
            m_head.SetHasEatDot(false);
            var newSnakeDot = new SnakeDot(CreateSnakeTaleGO(lastPosition.GetPosition()),lastPosition);
            snakeParts.Insert(1,newSnakeDot);
            newSnakeDot.SetHasEatDot();
        }
        else
        {
            m_head.SetHasEatDot(false);
            if (snakeParts.Count > 1)
            {
                var lastPart = snakeParts[snakeParts.Count-1];
                snakeParts.RemoveAt(snakeParts.Count-1);
                lastPart.MoveTo(lastPosition);
                snakeParts.Insert(1,lastPart);
            }
        }

        
    }

    public void EatDotOnCurrPos()
    {
        m_head.SetHasEatDot();
    }

    public List<GridPosition> GetUsedPartsPositions()
    {
        List<GridPosition> pos = new List<GridPosition>();
        snakeParts.ForEach(q=>pos.Add(q.GetPosition()));
        return pos;
    }

    public void AutoDestroy()
    {
        foreach (var snakePart in snakeParts)
        {
            snakePart.AutoDestroy();
        }

        snakeParts = null;
        m_head?.AutoDestroy();
        m_head = null;
        GameObject.Destroy(snakePartsGO);
    }
}