﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GridPosition
{
    private int x;
    private int y;
    private Vector3 position;
    private GameObject gridDotObject;
    private List<object> ObjectsOnPosition = new List<object>();

    public static bool Equals(GridPosition posA, GridPosition posB)
    {
        return posA.x == posB.x && posA.y==posB.y;
    }

    public GridPosition(int x, int y, Vector3 position, GameObject gridPosObj)
    {
        this.x = x;
        this.y = y;
        this.position = position;
        gridDotObject = gridPosObj;
    }

    public Vector3 GetPosition() => position;

    public Vector2Int GetGridPos() => new Vector2Int(x,y);

    public void AddObject(object go)
    {
        ObjectsOnPosition.Add(go);
    }

    public void RemoveObject(object go)
    {
        if (ObjectsOnPosition!=null && ObjectsOnPosition.Contains(go)) ObjectsOnPosition.Remove(go);
    }

    public bool IsOccupiedBy(Type type)
    {
        foreach (object obj in ObjectsOnPosition)
        {
            if (obj.GetType() == type) return true;
        }
        return false;
    }
    
    public bool IsOccupiedBy(object obj)
    {
        if (ObjectsOnPosition.Contains(obj)) return true;
        return false;
    }

    public void AutoDestroy()
    {
        GameObject.Destroy(gridDotObject);
        ObjectsOnPosition = null;
    }
}