﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    private float m_speed = 20f;
    private Vector2 m_currDirection;
    private bool m_isMoving;
    
    void Start()
    {
        m_currDirection = new Vector2(1,-1);
        m_isMoving = false;
    }

    public void SetSpeed(float speed)
    {
        m_speed = speed;
    }

    public void EnableMovement(bool enable = true)
    {
        m_isMoving = enable;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        ContactPoint2D contactPoint = other.contacts[0];
        Vector2 normal = contactPoint.normal;
        ISpeedAngleChanger otherModifier = other.transform.GetComponent<ISpeedAngleChanger>();
        if (otherModifier!=null)
        {
            normal = otherModifier.GetNormalModifier(normal);
        }
        m_currDirection = Vector2.Reflect(m_currDirection, normal);
    }

    private void Update()
    {
        if (m_isMoving)
        {
            Vector2 currPosition = transform.position;
            Vector2 requiredPosition = currPosition + m_currDirection; 
            transform.position = Vector2.Lerp(currPosition, requiredPosition, m_speed * Time.deltaTime);
        }
    }

    public void Reset(Vector2 position, Vector2 direction)
    {
        m_currDirection = new Vector2(1,-1);
        transform.position = position+Vector2.right;
    }
}
