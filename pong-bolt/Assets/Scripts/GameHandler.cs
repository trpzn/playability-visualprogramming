﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = System.Random;

public class GameHandler : MonoBehaviour
{
    // References
    public Canvas canvas;
    public UserMovement userMovement;
    public BotMovement botMovement;
    public BallBehaviour ball;
    public CollisionDetector leftWall;
    public CollisionDetector rightWall;

    // Handler data
    public float speed = 20f;
    private bool m_userLose = true;
    private int m_userScore = 0;
    private int maxUserScore => GetUserMaxScore();
    

    private void OnGoalUser()
    {
        ball.Reset(userMovement.GetUserPosition(),Vector2.zero);
        OnPoint(1);
    }

    private void OnGoalBot()
    {
        ball.Reset(userMovement.GetUserPosition(),Vector2.zero);
        OnPoint(-1);
    }

    private int GetUserMaxScore()
    {
        return !PlayerPrefs.HasKey("MAXSCORE") ? 0 : PlayerPrefs.GetInt("MAXSCORE");
    }

    private void SetUserMaxScore(int score)
    {
        PlayerPrefs.SetInt("MAXSCORE",score);
    }

    private void Start() {
        
        canvas.inGame.mainMenuAction = GoToMainMenuAction;
        canvas.newGame.newGameAction = NewGameAction;
        
        botMovement.SetBall(ball.transform);

        userMovement.SetSpeed(speed);
        botMovement.SetSpeed(speed);
        ball.SetSpeed(speed);
        leftWall.CollisionEnterEvent = OnGoalBot;
        rightWall.CollisionEnterEvent = OnGoalUser;

        GoToMainMenuAction();
    }

    private void NewGameAction()
    {
        m_userScore = 0;
        m_userLose = false;
        
        canvas.inGame.UpdateUserScore(m_userScore);
        canvas.inGame.UpdateMaxScore(GetUserMaxScore());
        
        canvas.inGame.Show();
        canvas.newGame.Show(false);
        
        botMovement.ResetMovement();
        botMovement.EnableMovement(true);
        userMovement.ResetMovement();
        userMovement.EnableMovement(true);
        ball.Reset(userMovement.GetUserPosition(),Vector2.zero);
        ball.EnableMovement(true);
    }

    private void GoToMainMenuAction()
    {
        if(m_userScore>=GetUserMaxScore()) SetUserMaxScore(m_userScore);
        canvas.newGame.SetMaxScore(GetUserMaxScore());
        
        m_userScore = 0;
        m_userLose = true;
        canvas.inGame.Show(false);
        canvas.newGame.Show();
        
        botMovement.ResetMovement();
        botMovement.EnableMovement(false);
        userMovement.ResetMovement();
        userMovement.EnableMovement(false);
        ball.Reset(userMovement.GetUserPosition(),Vector2.zero);
        ball.EnableMovement(false);
    }

    private void OnPoint(int point)
    {
        m_userScore += point;
        canvas.inGame.UpdateUserScore(m_userScore);
    }

    [Serializable]
    public class Canvas
    {
        public IngameCanvas inGame;
        public NewGameCanvas newGame;
    }
}