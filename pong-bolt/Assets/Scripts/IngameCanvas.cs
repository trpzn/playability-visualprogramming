﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class IngameCanvas : MonoBehaviour
{
    public GameObject rootGO;
    public Action mainMenuAction;
    public TextMeshProUGUI score;
    public TextMeshProUGUI maxScore;

    public void GoToMenuPressed()
    {
        //mainMenuAction?.Invoke();
    }

    public void Show(bool status = true)
    {
        //rootGO.SetActive(status);
    }

    public void UpdateUserScore(int mUserScore)
    {
        //score.text = mUserScore.ToString();
    }

    public void UpdateMaxScore(int userMaxScore)
    {
        //maxScore.text = userMaxScore.ToString();
    }
}
