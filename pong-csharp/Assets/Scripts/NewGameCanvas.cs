﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NewGameCanvas : MonoBehaviour
{
    public GameObject rootGO;
    public Action newGameAction;
    public TextMeshProUGUI maxScore;
    
    public void StartNewGame()
    {
        newGameAction?.Invoke();
    }
    
    public void Show(bool status = true)
    {
        rootGO.SetActive(status);
    }

    public void SetMaxScore(int mUserScore)
    {
        maxScore.text = mUserScore.ToString();
    }
}
