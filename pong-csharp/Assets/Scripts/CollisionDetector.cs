﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    public Action CollisionEnterEvent;

    private void OnCollisionEnter2D(Collision2D other)
    {
        CollisionEnterEvent?.Invoke();
    }
}
