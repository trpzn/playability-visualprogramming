﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserMovement : MonoBehaviour
{
    private bool keyUp;
    private bool keyDown;
    public PaddleMovement userPaddle;
    private bool m_isMoving;
    
    // Update is called once per frame
    void Update()
    {
        if(!m_isMoving) return;
        if (Input.GetKeyDown(KeyCode.UpArrow)) keyUp = true;
        if (Input.GetKeyUp(KeyCode.UpArrow)) keyUp = false;
        if (Input.GetKeyDown(KeyCode.DownArrow)) keyDown = true;
        if (Input.GetKeyUp(KeyCode.DownArrow)) keyDown = false;

        if (keyUp || keyDown)
        {
            if(keyUp) userPaddle.MoveTo(1);
            else if(keyDown) userPaddle.MoveTo(-1);
        }
        else userPaddle.MoveTo(0);
    }

    public void SetSpeed(float speed)
    {
        userPaddle.SetSpeed(speed);
    }

    public Vector2 GetUserPosition()
    {
        return userPaddle.transform.position;
    }
    
    public void EnableMovement(bool status)
    {
        m_isMoving = status;
        userPaddle.EnableMovement(status);
    }

    

    public void ResetMovement()
    {
        m_isMoving = false;
        userPaddle.ResetMovement();
    }
}
