﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleMovement : MonoBehaviour, ISpeedAngleChanger
{
    public float posMax;
    public float posMin;

    private bool isMoving = false;
    float currMove;
    private BoxCollider2D box;
    private float speed = 10f;
    private float directionMod = 0.2f;
    private Vector2 m_initPos;

    private void Awake()
    {
        box = GetComponent<BoxCollider2D>();
        m_initPos = transform.position;
    }

    public void MoveTo(int movement)
    {
        if (
            (transform.position.y <= posMax) &&
            (transform.position.y >= posMin)
        )
        {
            if(isMoving && currMove == movement) return;
            if(!isMoving && movement == 0) return;
            if (movement == 0)
            {
                isMoving = false;
                currMove = 0;
                return;
            }

            isMoving = true;
            currMove = movement;
        }
        else
        {
            if (isMoving)
            {
                currMove = 0;
                isMoving = false;
            }
        }
    }

    public void Update()
    {
        if (isMoving)
        {
            Vector3 currPosition = transform.position;
            Vector3 requiredPosition = currPosition + new Vector3(0,currMove, 0);
            Vector3 endPos = Vector3.Lerp(currPosition, requiredPosition, speed * Time.deltaTime);
            if (endPos.y > posMax) endPos.y = posMax;
            if (endPos.y < posMin) endPos.y = posMin;
            
            transform.position = endPos;
        }
    }

    public float GetWidth()
    {
        return box.size.y*transform.localScale.y;
    }

    public Vector2 GetNormalModifier(Vector2 normal)
    {
        if (normal.y != 0 || normal.x == 0) return normal;
        //Debug.Log(normal);
        //Debug.Log(directionMod * currMove);
        if (normal.x > 0)
        {
            normal -= new Vector2(0,directionMod * currMove);
        }
        
        //Debug.Log(normal.normalized);
        return normal.normalized;
    }

    public void SetSpeed(float f)
    {
        speed = f;
    }

    public void ResetMovement()
    {
        isMoving = false;
        transform.position = m_initPos;
    }

    public void EnableMovement(bool status)
    {
        isMoving = status;
    }
}

public interface ISpeedAngleChanger
{
    Vector2 GetNormalModifier(Vector2 normal);
}
