﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotMovement : MonoBehaviour
{
    private Transform m_ballTransform;
    public PaddleMovement paddle;
    public float paddleRadius;
    private bool m_isMoving;
    
    // Start is called before the first frame update
    public void SetBall(Transform ballTransform)
    {
        m_ballTransform = ballTransform;
        paddleRadius = paddle.GetWidth()/2;
    }

    private void Update()
    {
        if (m_isMoving)
        {
            Vector2 paddlePosition = paddle.transform.position;
            Vector2 ballPosition = m_ballTransform.position;
            
            if (ballPosition.y > paddlePosition.y+paddleRadius)
            {
                paddle.MoveTo(1);
            }
            else if(ballPosition.y < paddlePosition.y-paddleRadius)
            {
                paddle.MoveTo(-1);
            }
        }
    }

    public void EnableMovement(bool status)
    {
        m_isMoving = status;
        paddle.EnableMovement(status);
    }

    public void ResetMovement()
    {
        m_isMoving = false;
        paddle.ResetMovement();
    }

    public void SetSpeed(float speed)
    {
        paddle.SetSpeed(speed);
    }
}
