﻿using UnityEngine;

public class EatDot
{
    public GridPosition pos;
    public GameObject gameObject;

    public EatDot(GridPosition gridPosition, Sprite assetsWhiteDot)
    {
        //Debug.Log(this);
        gameObject = new GameObject("Dot");
        pos = gridPosition;
        SpriteRenderer sprite = gameObject.AddComponent<SpriteRenderer>();
        sprite.sprite = assetsWhiteDot;
        sprite.sortingOrder = 1;
        gameObject.transform.localScale = Vector3.one * 2f;
        gameObject.transform.position = pos.GetPosition();
        pos.AddObject(this);
    }

    public void MoveToPos(GridPosition posOnGrid)
    {
        pos?.RemoveObject(this);
        pos = posOnGrid;
        pos.AddObject(this);
        gameObject.transform.position = pos.GetPosition();
    }

    public void AutoDestroy()
    {
        pos?.RemoveObject(this);
        pos = null;
        GameObject.Destroy(gameObject);
    }
}