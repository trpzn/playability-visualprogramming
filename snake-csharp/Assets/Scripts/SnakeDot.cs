﻿using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class SnakeDot
{
    private GameObject dotObject;
    private GridPosition currGridPos;
    public bool isEatDot => m_isEatDot;
    private bool m_isEatDot;

    public SnakeDot(GameObject go, GridPosition initialPos)
    {
        dotObject = go;
        currGridPos = initialPos;
    }

    public GridPosition GetPosition() => currGridPos;

    public void MoveTo(GridPosition gp)
    {
        currGridPos?.RemoveObject(this);
        currGridPos = gp;
        currGridPos.AddObject(this);
        dotObject.transform.position = gp.GetPosition();
    }

    public void SetHasEatDot(bool set = true)
    {
        m_isEatDot = set;
    }

    public void AutoDestroy()
    {
        GameObject.Destroy(dotObject);
        currGridPos?.RemoveObject(this);
        currGridPos = null;
    }
}